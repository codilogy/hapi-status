const os = require('os')

exports.register = (server, options, next) => {
  server.route({
    method: ['GET'],
    path: `/status`,
    config: {
      auth: false,
      handler: (request, reply) => {
        reply({
          host: os.hostname(),
          user: os.userInfo(),
          cpu: os.cpus(),
          network: os.networkInterfaces(),
          memory: os.totalmem()
        }).code(200)
      }
    }
  })

  next()
}

exports.register.attributes = {
  pkg: require('./package.json')
}
