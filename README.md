# hapi-status

[![JavaScript Standard Style](https://img.shields.io/badge/code_style-standard-green.svg)](https://standardjs.com/)
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-green.svg)](https://conventionalcommits.org/)
[![bitHound Overall Score](https://www.bithound.io/bitbucket/codilogy/hapi-status/badges/score.svg)](https://www.bithound.io/bitbucket/codilogy/hapi-status)
[![Coverage Status](https://coveralls.io/repos/bitbucket/codilogy/hapi-status/badge.svg)](https://coveralls.io/bitbucket/codilogy/hapi-status)


> Opinionated hapi plugin for injecting server status into your API.

## License
